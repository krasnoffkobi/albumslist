import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  public getAlbumsList() {
    return this.http.get('/assets/json/albumslist.json').toPromise();
  }

  public getAlbumsDetails(albumId: number) {
    return this.http.get('/assets/json/details.json').toPromise();
  }
}
