import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from '../app.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  details: any;
  albumId: number;

  constructor(private route: ActivatedRoute, public appService: AppService) {
    route.paramMap.subscribe(params => {
      console.log(params.get('albumid'));
    });
  }

  ngOnInit() {
    this.init();
  }

  async init() {
    this.details = await this.appService.getAlbumsDetails(this.albumId);
  }

}
