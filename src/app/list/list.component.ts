import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  albums: any;
  albumsDetails: string;

  constructor(public appService: AppService, private router: Router) { }

  ngOnInit() {
    this.init();
  }

  async init() {
    this.albums = await this.appService.getAlbumsList();
  }

  mouseEnter(albumId) {
    console.log('mouseEnter', albumId);
    this.albumsDetails = this.albums.find(el => el.albumId === albumId).Details;
  }

  mouseLeave(albumId) {
    console.log('mouseLeave', albumId);
    this.albumsDetails = ' ';
  }

  mouseClick(albumId) {
    this.router.navigate(['/albumDetails/', albumId]);
  }
}
