import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { AlbumDetailsComponent } from './album-details/album-details.component';


const routes: Routes = [
    { path: 'list', component: ListComponent },
    { path: 'albumDetails/:albumid', component: AlbumDetailsComponent },
    { path: '',   redirectTo: '/list', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

 }